const toml = require('toml')
const glob = require('glob')
const path = require('path')
const fs = require('fs')
const { VError } = require('verror')
const events = []

for (const file of glob.sync('data/**/*.toml')) {
  try {
    const data = toml.parse(fs.readFileSync(file, 'utf8'))
    const date = path.basename(file).match(/^\d+-\d+-\d+/)[0]
    const slug = path.basename(file, '.toml')
    if (!Array.isArray(data.party)) {
      throw new Error('Did not find an array named "party".')
    }
    events.push({ ...data, date, slug, source: file })
    console.log('* Processed', file)
  } catch (e) {
    throw new VError(
      e,
      'Cannot process "%s"%s',
      file,
      e.line ? ` at line ${e.line}` : ''
    )
  }
}

events.sort((a, b) => (a.date < b.date ? 1 : -1))

fs.writeFileSync(
  'public/bingsu.js',
  'BingsuJS.events = ' + JSON.stringify(events, null, 2),
  'utf8'
)
